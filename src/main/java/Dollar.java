class Dollar extends Money{
    private String currency;

    Dollar(int amount, String currency) {
        super(amount, currency);
    }
    
    static Money dollar(int amount) {
        return new Money(amount, "USD");
    }
}
